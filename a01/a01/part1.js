function p1() {

  const fractions = require('./fractions');

  let exp; // the expected value
  let act; // the actual value
  let type; // test type

  function assert(exp, act, type) {
    console.assert(exp.num === act.num && exp.denom === act.denom,
      't=%s exp=%o act=%o', type, exp, act);
  }

  console.log("Part 1 begin");

  // add
  type = 'add';
  exp = { num: 3, denom: 4 };
  act = fractions.add({num:1, denom:2}, {num:1,denom:4});
  assert(exp, act, type);

  exp = { num: 3, denom: 2 };
  act = fractions.add({num:3, denom:4}, {num:3,denom:4});
  assert(exp, act, type);

  // subtract
  type = 'sub';
  exp = { num: 1, denom: 4 };
  act = fractions.subtract({num:1, denom:2}, {num:1,denom:4});
  assert(exp, act, type);

  exp = { num: -1, denom: 2 };
  act = fractions.subtract({num:1, denom:4}, {num:3,denom:4});
  assert(exp, act, type);

  // mult
  type = 'mult';
  exp = { num: 3, denom: 16 };
  act = fractions.multiply({num:1, denom:4}, {num:3,denom:4});
  assert(exp, act, type);

  exp = { num: 1, denom: 1 };
  act = fractions.multiply({num:2, denom:3}, {num:3,denom:2});
  assert(exp, act, type);

  //div
  type = 'div';
  exp = {num:4, denom:9};
  act = fractions.divide({num:2, denom:3}, {num:3, denom:2});
  assert(exp, act, type);

  exp = {num:-7, denom:6};
  act = fractions.divide({num:2, denom:3}, {num:-4, denom:7});
  assert(exp, act, type);

  // inverse
  type = 'inverse';
  exp = {num:3, denom:2};
  act = fractions.inverse({num:2, denom:3});
  assert(exp, act, type);

  exp = {num:-3, denom:2};
  act = fractions.inverse({num:-2, denom:3});
  assert(exp, act, type);

  // equals
  type = 'equals';
  console.assert(fractions.equals({num:1, denom:2}, {num:10, denom:20}));
  console.assert(fractions.equals({num:1, denom:2}, {num:-2, denom:-4}));
  console.assert(!fractions.equals({num:1, denom:2}, {num:2, denom:1}));
  console.assert(!fractions.equals({num:1, denom:2}, {num:-1, denom:2}));

  console.log("Part 1 end");
}

module.exports = p1;
