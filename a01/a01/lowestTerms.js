function gcd(n1, n2) {
  let a = Math.abs(n1);
  let b = Math.abs(n2);
  // https://en.wikipedia.org/wiki/Euclidean_algorithm
  if(isNaN(a) || isNaN(b)) throw new Error(`${o.num} ${o.denom}`);
  while (a != b) {
    if (a > b) {
      a = a - b;
    } else {
      b = b - a;
    }
  }
  return a;
}

function lowestTerms(o) {
  const g = gcd(o.num, o.denom);
  const f = {
    num: o.num / g,
    denom: o.denom / g,
  }
  if (f.denom < 0 ) {
    f.num = 0 - f.num;
    f.denom = 0 - f.denom;
  }
  return Object.freeze(f);
}

module.exports = lowestTerms;
