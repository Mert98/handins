# a01: Basics: Objects & git

*  You may only modify ```fractions.js```, and uncomment the two lines in ```main.js```.
*  Do not modify ```part1.js```, ```part2.js```, or ```lowestTerms.js```
*  The program should be runnable as: "node main.js"
*  You may not use any dependencies beyond the provided files

## Part 0:
*  commit these files to your ```a01``` handins folder (without modifying them)
*  you now have a baseline, and should not have to d/l them again

## Part 1:
*  implement basic fraction opperations in fractions.js
   *  ```add```, ```subtract```, ```multiply```, ```divide```, ```inverse``` & ```equals```
   *  all arguments provided to these functions will be fraction objects
   *  every fraction has two attributes: num (the numerator) and denom (the denominator)
   *  num and denom must be integers
   *  fractions should be treated as [immutable objects](https://en.wikipedia.org/wiki/Immutable_object)
      * functions should never change a fraction object
      * they should return a new fraction object instead
   *  the fractions that your functions return should always:
      *  be in [lowest terms](https://www.merriam-webster.com/dictionary/lowest%20terms)
      *  only the numerator may be negative, the denominator shall never be negative
      *  use the provided ```lowestTerms.js``` module to achieve this
      *  your functions must accept fractions which do not satisfy these rules
   *  two fractions are considered equal if, and only if, they evaluate to the same numeric value
      *  ie:  -2/-2 = 1/1
*  when you have completed part one, make a commit

## Part 2:
*  enhance your fraction functions to accept numbers in addition to fraction objects
   *  any argument passed to one of your functions may now be an integer
*  when you are done part 2, make a commit
*  then push
